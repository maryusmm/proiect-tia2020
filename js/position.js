var coordinates = [
    {
        "id": 1,
        "creationTime": "2020-12-17T09:47:22.497+0000",
        "terminalId": "200",
        "latitude": "46.7341",
        "longitude": "23.59312"
    },
    {
        "id": 2,
        "creationTime": "2020-12-17T09:49:42.092+0000",
        "terminalId": "devops",
        "latitude": "46.7783",
        "longitude": "23.6032"
    },
    {
        "id": 3,
        "creationTime": "2020-12-17T09:49:54.928+0000",
        "terminalId": "devops",
        "latitude": "46.7781",
        "longitude": "23.5822"
    }
];

//var coordinatesJson = JSON.parse(coordinates);

function getPositions() {
    var criteria = new Criteria();
    var entriesFound = 0;
    
    if (criteria.endDate < criteria.startDate)
    {
        
        swal("ERROR!", "Start date must be before end date!", "error")

    }else
    {for(var entry in coordinates)
    {   
        
       if(coordinates[entry].creationTime <= criteria.endDate && coordinates[entry].creationTime >= criteria.startDate && coordinates[entry].terminalId === criteria.terminalId)
       { 
           data={
            'lat':parseFloat(coordinates[entry].latitude),
            'lng':parseFloat(coordinates[entry].longitude)
        }
        entriesFound=1;
        const title = 'terminal: ' + coordinates[entry].terminalId + '\nDate Time ' + coordinates[entry].creationTime;
        addOnCoordinatesOnMap(data,title);
    }

    
    }if(entriesFound == 0)
        swal("Sorry!", "No entries found matching your criteria", "warning")

}}

function Criteria() {
    var deviceId = $('#deviceId').val().trim(); // select data from input and trim it
    if (deviceId.length > 0) {
        this.terminalId = deviceId;
    }

    var startDate = $('#startDate').val().trim(); // select data from input and trim it
    if (startDate.length > 0) {
        this.startDate = startDate;
    }

    var endDate = $('#endDate').val().trim(); // select data from input and trim it
    if (endDate.length > 0) {
        this.endDate = endDate;
    }
}
