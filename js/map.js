
var map;
var myLatLng = {lat: 46.7693924, lng: 23.5902006};
var markerArray=[];

function initialize() {
    var mapCanvas = document.getElementById('map');
    var mapOptions = {
        center: new google.maps.LatLng(myLatLng),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    map = new google.maps.Map(mapCanvas, mapOptions)
}

function addOnCoordinatesOnMap(data,title) {
    //title="terminal id:" +titl;

    var marker=new google.maps.Marker ({
        position:data,
        map:map,
        title:title
    });
    markerArray.push(marker);
}

function clearMap(){
    if(markerArray)
    for (var i = 0; i < markerArray.length; i++ ) {

        markerArray[i].setMap(null);
    }
    markerArray.length = 0;
}
function addStaticMarker() {
    var pos = getRandomPosition()
    var marker = new google.maps.Marker({
        position: pos,
        map: map,
        title: 'Hello World!'
    });
}

function getRandomPosition(){
    var randLatLng = {lat: (myLatLng["lat"] + Math.floor(Math.random() * 5) + 1),
        lng: (myLatLng["lng"] + Math.floor(Math.random() * 5) + 1)};
    return randLatLng;
}