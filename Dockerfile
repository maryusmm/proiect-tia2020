FROM nginx:alpine

RUN rm -f /usr/share/nginx/html/*
COPY /* /usr/share/nginx/html/
COPY js/* /usr/share/nginx/html/js/

EXPOSE 80